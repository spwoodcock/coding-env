# Coding Env

A pre-built containerised coding environment I use daily.

Includes: Python, Node, Golang, Docker, Kubectl, Helm...
