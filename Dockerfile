# Collect binaries
ARG INTERNAL_REG
ARG EXTERNAL_REG
ARG PYTHON_VERSION
ARG CODE_SERVER_TAG
ARG NODE_VERSION
FROM ${EXTERNAL_REG}/node:${NODE_VERSION}-bullseye-slim AS node-img



FROM ${EXTERNAL_REG}/codercom/code-server:${CODE_SERVER_TAG} AS code-server
ARG COPILOT
RUN code-server --install-extension ms-python.python && \
    code-server --install-extension ms-pyright.pyright && \
    code-server --install-extension golang.go && \
    code-server --install-extension mikestead.dotenv && \
    code-server --install-extension redhat.vscode-yaml && \
    code-server --install-extension antfu.vite && \
    code-server --install-extension dbaeumer.vscode-eslint && \
    code-server --install-extension antfu.iconify && \
    code-server --install-extension lokalise.i18n-ally && \
    code-server --install-extension vscode-icons-team.vscode-icons && \
    code-server --install-extension svelte.svelte-vscode && \
    code-server --install-extension tobermory.es6-string-html && \   
    code-server --install-extension skellock.just && \
    if [ "$COPILOT" = "false" ]; then \
        echo "Not installing copilot." ; \
    else \
        curl -L \
        "https://marketplace.visualstudio.com/_apis/public/gallery/publishers/GitHub/vsextensions/copilot/latest/vspackage" \
            -o github.copilot.vsix.gz && \
        gunzip -v github.copilot.vsix.gz && \
        code-server --install-extension ./github.copilot.vsix && \
        rm -rf github.copilot.vsix ; \
    fi


FROM ${EXTERNAL_REG}/python:${PYTHON_VERSION}-slim-bookworm AS dependencies
ARG PYTHON_VERSION
ARG UV_VERSION
ARG GOLANG_VERSION
WORKDIR /opt/python
RUN set -ex \
    && apt-get update \
    && apt-get install -y --no-install-recommends \
        curl \
    && rm -rf /var/lib/apt/lists/*
# Python dependencies
RUN \
    curl -LsSf "https://astral.sh/uv/$UV_VERSION/install.sh" | sh \
    && /root/.local/bin/uv pip install --system \
        "uv==${UV_VERSION}" pdm pre-commit commitizen
WORKDIR /opt/golang
# Install Golang (cannot add MUSL / Alpine compiled Go binary!) present in /opt/golang/go/bin/go
RUN curl -fsSL "https://golang.org/dl/go${GOLANG_VERSION}.linux-amd64.tar.gz" | tar xz -C .



FROM ${EXTERNAL_REG}/python:${PYTHON_VERSION}-slim-bookworm AS coding-env

ARG APP_USER
ARG PYTHON_VERSION
ARG PNPM_VERSION

SHELL ["/bin/bash", "-c"]

RUN set -ex \
    && RUN_DEPS=" \
    wget \
    curl \
    gosu \
    fish \
    htop \
    man \
    nano \
    git \
    procps \
    openssh-client \
    sudo \
    lsb-release \
    apt-transport-https \
    gnupg-agent \
    gnupg2 \
    libpq-dev \
    build-essential \
    libbtrfs-dev \
    libgpgme-dev \
    libdevmapper-dev \
    pkg-config \
    " \
    && apt-get update \
    && apt-get install -y --no-install-recommends $RUN_DEPS \
    # Add Prebuilt-MPR repository (user pre-built binaries)
    && wget -qO - 'https://proget.makedeb.org/debian-feeds/prebuilt-mpr.pub' \
        | gpg --dearmor | tee /usr/share/keyrings/prebuilt-mpr-archive-keyring.gpg > /dev/null \
    && echo "deb [arch=all,$(dpkg --print-architecture) signed-by=/usr/share/keyrings/prebuilt-mpr-archive-keyring.gpg] https://proget.makedeb.org prebuilt-mpr $(lsb_release -cs)" \
        | tee /etc/apt/sources.list.d/prebuilt-mpr.list \
    && apt update && apt install "just" \
    && rm -rf /var/lib/apt/lists/*

# Add entrypoint
ADD docker-entrypoint.sh /docker-entrypoint.sh

# Add NodeJS + NPM
COPY --from=node-img \
    /usr/local/bin/node /usr/local/bin/node
COPY --from=node-img \
    /usr/local/lib/node_modules /usr/local/lib/node_modules
# Add installed binaries (Python)
COPY --from=dependencies \
    /usr/local/bin /usr/local/bin
# Add Python libs
COPY --from=dependencies \
    "/usr/local/lib/python$PYTHON_VERSION" "/usr/local/lib/python$PYTHON_VERSION"
# Golang binaries
# NOTE do not put the 'go' binary directly in /usr/local/bin/ (https://go.dev/doc/install)
COPY --from=dependencies \
    /opt/golang/go /usr/local/
# Install all other deps & configs
RUN set -euo pipefail \
    # Create non-root user to run the app
    && adduser --gecos '' --disabled-password ${APP_USER} \
    # Config NPM & NPX
    && ln -s /usr/local/bin/node /usr/local/bin/nodejs \
    && ln -s /usr/local/lib/node_modules/npm/bin/npm-cli.js /usr/local/bin/npm \
    && ln -s /usr/local/lib/node_modules/npm/bin/npx-cli.js /usr/local/bin/npx \
    && npm install --location=global "pnpm@${PNPM_VERSION}" @quasar/cli \
    # Permissions & executable entrypoint
    && chown -R ${APP_USER} /opt \
    && chmod -R 777 /opt \
    && chown -R $APP_USER:$APP_USER "/home/${APP_USER}" \
    && chmod +x /docker-entrypoint.sh

# # Install QGIS
# RUN curl -sL https://qgis.org/downloads/qgis-2021.gpg.key | gpg --import - && \
#     gpg --fingerprint 46B5721DBBD2996A && \
#     gpg --export --armor 46B5721DBBD2996A | apt-key add - && \
#     echo "deb https://qgis.org/debian-ltr $(lsb_release -cs) main" >> /etc/apt/sources.list && \
#     apt-get update && \
#     apt-get install -y --no-install-recommends \
#     qgis \
#     python3-qgis \
#     qgis-common \
#     qgis-providers \
#     && rm -rf /var/lib/apt/lists/*
# # Set dummy virtual display as USER (for PyQt5/qgis)
# # x11dummy volume mounted, set DISPLAY to match
# ENV DISPLAY=:1

# Add code-server & extensions
COPY --from=code-server \
    /usr/bin/code-server \
    /usr/bin/code-server
COPY --from=code-server \
    /usr/lib/code-server \
    /usr/lib/code-server
COPY --from=code-server --chown=${APP_USER} \
    /home/coder/.local/share/code-server/extensions/ \
    "/home/$APP_USER/vscode-conf/extensions/"



# NOTE here we squash the image to be as minimal as possible
FROM scratch
ARG APP_USER
ARG CODE_SERVER_TAG
ARG PYTHON_VERSION
ARG UV_VERSION
ARG NODE_VERSION
ARG PNPM_VERSION
ARG GOLANG_VERSION
ARG MAINTAINER
LABEL "APP_USER"="${APP_USER}" \
      "CODE_SERVER_VERSION"="${CODE_SERVER_TAG}" \
      "PYTHON_VERSION"="${PYTHON_VERSION}" \
      "UV_VERSION"="${UV_VERSION}" \
      "NODE_VERSION"="${NODE_VERSION}" \
      "PNPM_VERSION"="${PNPM_VERSION}" \
      "GOLANG_VERSION"="${GOLANG_VERSION}" \
      "MAINTAINER"="${MAINTAINER}" \
      "APP_PORT"="8888"
ENV SHELL=/usr/bin/fish \
    PATH=$PATH:/usr/local/go/bin
COPY --from=coding-env / /
WORKDIR /home/${APP_USER}
# Entrypoint script, passing commands to script as $@
ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["/usr/bin/code-server", "--bind-addr", "0.0.0.0:8888", \
     "--auth", "none", "--disable-telemetry", "--disable-update-check"]
