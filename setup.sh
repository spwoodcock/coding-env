#!/bin/bash

echo "Setup Traefik"
docker network create traefik || true
cd traefik
source gen_cert.sh
docker compose up -d

echo "Setup code-server"
cd ..
docker volume create vscode-conf
docker compose up -d
