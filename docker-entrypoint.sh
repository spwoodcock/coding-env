#!/bin/bash

set -eo pipefail

if [[ ! -z "${GRANT_SUDO}" ]]; then
    echo "Granting ${APP_USER} user sudo access"
    echo "${APP_USER} ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/nopasswd
fi

# Git config from env vars
echo "Setting user config (Git, PNPM)"
gosu "${APP_USER}" git config --global credential.helper store
gosu "${APP_USER}" git config --global pull.rebase false
gosu "${APP_USER}" git config --global user.name "$GIT_USER"
gosu "${APP_USER}" git config --global user.email "$GIT_EMAIL"
gosu "${APP_USER}" pnpm config set auto-install-peers true

echo "Starting code-server as user ${APP_USER}"
gosu "${APP_USER}" "$@" --user-data-dir "/home/${APP_USER}/vscode-conf" \
     --extensions-dir "/home/${APP_USER}/vscode-conf/extensions"
